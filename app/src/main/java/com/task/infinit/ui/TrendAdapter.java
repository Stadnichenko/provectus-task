package com.task.infinit.ui;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.task.infinit.api.model.Trend;

import java.util.ArrayList;
import java.util.List;

public class TrendAdapter extends RecyclerView.Adapter<TrendViewHolder> {

    private final List<Trend> mTrends = new ArrayList<>();

    public void setData(List<Trend> trends) {
        mTrends.clear();
        mTrends.addAll(trends);

        notifyDataSetChanged();
    }

    @Override
    public TrendViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return TrendViewHolder.newInstance(parent);
    }

    @Override
    public void onBindViewHolder(TrendViewHolder holder, int position) {
        holder.bind(mTrends.get(position));
    }

    @Override
    public int getItemCount() {
        return mTrends.size();
    }

}