package com.task.infinit.ui;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.task.infinit.R;
import com.task.infinit.api.model.Trend;

public class TrendViewHolder extends RecyclerView.ViewHolder {

    private Context mContext;

    private final TextView mCreator;
    private final TextView mTrendTitle;
    private final ImageView mImageBlur;

    static TrendViewHolder newInstance(ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_trend, parent, false);

        return new TrendViewHolder(parent.getContext(), view);
    }

    private TrendViewHolder(Context context, View view) {
        super(view);

        mContext = context;

        mCreator = (TextView) view.findViewById(R.id.creator);
        mTrendTitle = (TextView) view.findViewById(R.id.title);

        mImageBlur = (ImageView) view.findViewById(R.id.blur);
    }

    void bind(Trend trend) {
        if (trend.getCreator() != null) {
            mCreator.setText(trend.getCreator());
        }

        mTrendTitle.setText(trend.getTitle());

        Picasso.with(mContext).load(trend.getImageBlur()).into(mImageBlur);
    }

}