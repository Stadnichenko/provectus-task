package com.task.infinit.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.task.infinit.R;
import com.task.infinit.api.Api;
import com.task.infinit.api.model.Trend;
import com.task.infinit.rx.RequestSubscriber;
import com.task.infinit.rx.SingleTransformers;
import com.task.infinit.util.NetworkUtils;
import com.task.infinit.util.recycler.ItemClickListener;
import com.task.infinit.util.recycler.ItemDecoration;

import java.util.List;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;
import timber.log.Timber;

public class TrendFragment extends Fragment {

    private final CompositeSubscription mSubscriptions = new CompositeSubscription();
    private TrendAdapter mAdapter;

    public static TrendFragment newInstance() {
        return new TrendFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new TrendAdapter();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_trend, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        RecyclerView recycler = (RecyclerView) view.findViewById(R.id.recycler_view_list);
        recycler.setLayoutManager(new LinearLayoutManager(getActivity()));

        recycler.addItemDecoration(
                new ItemDecoration(getActivity(), LinearLayoutManager.VERTICAL)
        );

        recycler.addOnItemTouchListener(
                new ItemClickListener(getContext(), mItemClickListener)
        );

        recycler.setAdapter(mAdapter);

        loadBrandsFromNetwork();
    }

    private void loadBrandsFromNetwork() {
        if (!NetworkUtils.isNetworkAvailable(getContext())) {
            return;
        }

        Subscription subscription = Api.get()
                .getTrends()
                .compose(SingleTransformers.applySchedulers())
                .subscribe(mRequestListener);

        mSubscriptions.add(subscription);
    }

    private final RequestSubscriber<List<Trend>> mRequestListener =
            new RequestSubscriber<List<Trend>>() {

                @Override
                public void onSuccess(List<Trend> trends) {
                    mAdapter.setData(trends);
                }

                @Override
                public void onError(Throwable error) {
                    Timber.w(error, "Load trends from network.");
                }
            };

    private final ItemClickListener.OnItemClickListener mItemClickListener =
            (view, position) -> {
                Timber.i("click" + position);
            };
}