package com.task.infinit.util;

public interface Config {

    interface Api {
        String URL = "http://geekinformer.net/api/db/items/";

    }

    interface Http {
        int DEFAULT_CONNECT_TIMEOUT_MILLIS = 45 * 1000;
        int DEFAULT_READ_TIMEOUT_MILLIS = 60 * 1000;
        int DEFAULT_WRITE_TIMEOUT_MILLIS = 60 * 1000;

        int MAX_DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50 MB
    }
}