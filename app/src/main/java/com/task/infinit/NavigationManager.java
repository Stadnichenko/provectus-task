package com.task.infinit;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

public class NavigationManager {

    private static final NavigationManager STUB = new StubNavigationManager(null);

    private static NavigationManager mInstance;

    private final FragmentActivity mActivity;

    public static void create(FragmentActivity activity) {
        mInstance = new NavigationManager(activity);
    }

    public static void destroy(FragmentActivity activity) {
        if (mInstance.mActivity == activity) {
            mInstance = null;
        }
    }

    public static NavigationManager get() {
        if (mInstance == null) {
            return STUB;
        }

        return mInstance;
    }

    private NavigationManager(FragmentActivity activity) {
        mActivity = activity;
    }

    public void add(Fragment fragment) {
        add(fragment, null);
    }

    public void add(Fragment fragment, String tag) {
        FragmentManager manager = mActivity.getSupportFragmentManager();
        Fragment currentFragment = manager.findFragmentById(R.id.content_container);

        FragmentTransaction transaction = manager.beginTransaction();

        if (currentFragment != null) {
            transaction.hide(currentFragment);
        }

        transaction.add(R.id.content_container, fragment, tag);
        transaction.commit();
    }

    private static class StubNavigationManager extends NavigationManager {

        private StubNavigationManager(FragmentActivity activity) {
            super(activity);
        }

        @Override
        public void add(Fragment fragment) {
            // nothing to do
        }
    }
}