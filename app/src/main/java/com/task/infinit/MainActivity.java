package com.task.infinit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.task.infinit.ui.TrendFragment;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onDestroy() {
        NavigationManager.destroy(this);

        super.onDestroy();
    }

    @Override
    public void onContentChanged() {
        super.onContentChanged();

        NavigationManager.create(this);
        NavigationManager.get().add(TrendFragment.newInstance());
    }

}