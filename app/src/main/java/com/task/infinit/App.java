package com.task.infinit;

import android.app.Application;
import android.os.Handler;
import android.os.StrictMode;

import com.task.infinit.api.Api;

import timber.log.Timber;

public class App extends Application {

    private static App sInstance;

    public static App get() {
        return sInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        configureLogger();
        configureStrictMode();

        new Thread(Api::get).start();
    }

    private void configureLogger() {
        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    private void configureStrictMode() {
        if (BuildConfig.DEBUG) {
            new Handler().postAtFrontOfQueue(StrictMode::enableDefaults);
        }
    }

}