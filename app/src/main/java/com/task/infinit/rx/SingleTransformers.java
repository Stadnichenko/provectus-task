package com.task.infinit.rx;

import rx.Single;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public final class SingleTransformers {

    private static final Single.Transformer<Object, Object> SCHEDULERS =
            single -> single.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    /**
     * Apply execute task in background thread and notification in main thread
     * Please use this transformer before subscribe method only.
     */
    @SuppressWarnings("unchecked")
    public static <T> Single.Transformer<T, T> applySchedulers() {
        return (Single.Transformer<T, T>) SCHEDULERS;
    }

    private SingleTransformers() {
        throw new AssertionError("No instance.");
    }

}
