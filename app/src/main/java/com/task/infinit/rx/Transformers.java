package com.task.infinit.rx;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public final class Transformers {

    private static final Observable.Transformer<Object, Object> SCHEDULERS =
            observable -> observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread());

    /**
     * Apply execute task in background thread and notification in main thread
     * Please use this transformer before subscribe method only.
     */
    @SuppressWarnings("unchecked")
    public static <T> Observable.Transformer<T, T> applySchedulers() {
        return (Observable.Transformer<T, T>) SCHEDULERS;
    }

    private Transformers() {
        throw new AssertionError("No instance.");
    }

}
