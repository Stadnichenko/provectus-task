package com.task.infinit.rx;

import rx.SingleSubscriber;

public class RequestSubscriber<T> extends SingleSubscriber<T> {

    @Override
    public void onSuccess(T value) {
    }

    @Override
    public void onError(Throwable error) {
    }

}