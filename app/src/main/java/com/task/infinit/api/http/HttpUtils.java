package com.task.infinit.api.http;

import android.content.Context;

import com.task.infinit.BuildConfig;
import com.task.infinit.util.Config;

import java.io.File;
import java.util.concurrent.TimeUnit;

import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;


public final class HttpUtils {

    public static OkHttpClient createDefaultClient(Context context) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        builder.retryOnConnectionFailure(true);

        builder.connectTimeout(Config.Http.DEFAULT_CONNECT_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        builder.readTimeout(Config.Http.DEFAULT_READ_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);
        builder.writeTimeout(Config.Http.DEFAULT_WRITE_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);

        File cacheDir = createDefaultCacheDir(context);
        if (cacheDir != null) {
            builder.cache(new Cache(cacheDir, Config.Http.MAX_DISK_CACHE_SIZE));
        }

        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor logger = new HttpLoggingInterceptor(new TimberHttpLogger());
            logger.setLevel(HttpLoggingInterceptor.Level.BODY);
            builder.interceptors().add(logger);
        }

        return builder.build();
    }

    private static File createDefaultCacheDir(Context context) {
        if (context == null) {
            return null;
        }

        File cacheDir = context.getCacheDir();
        if (cacheDir == null) {
            return null;
        }

        File cache = new File(cacheDir, "http");
        if (!cache.mkdirs() && !cache.exists()) {
            Timber.w("Cannot make directory for http cache.");
            return null;
        }

        return cache;
    }

    private HttpUtils() {
        throw new AssertionError("No instance.");
    }
}
