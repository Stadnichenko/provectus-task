package com.task.infinit.api.model;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

@JsonObject
public class Trend {

    @JsonField(name = "id")
    String mId;

    @JsonField(name = "title")
    String mTitle;

    @JsonField(name = "creator")
    String mCreator;

    @JsonField(name = "image_blur")
    String mImageBlur;

    @JsonField(name = "image_thumbnail")
    String mImageThumbnail;

    public String getId() {
        return mId;
    }

    public String getTitle() {
        return mTitle;
    }

    public String getCreator() {
        return mCreator;
    }

    public String getImageBlur() {
        return mImageBlur;
    }

    public String getImageThumbnail() {
        return mImageThumbnail;
    }

    @Override
    public String toString() {
        return "Trend{" +
                "mId='" + mId + '\'' +
                ", mTitle='" + mTitle + '\'' +
                ", mCreator='" + mCreator + '\'' +
                ", mImageBlur='" + mImageBlur + '\'' +
                ", mImageThumbnail='" + mImageThumbnail + '\'' +
                '}';
    }
}