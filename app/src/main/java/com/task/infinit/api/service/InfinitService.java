package com.task.infinit.api.service;

import com.task.infinit.api.model.Trend;

import java.util.List;

import retrofit2.http.GET;
import rx.Single;

public interface InfinitService {

    @GET("trends")
    Single<List<Trend>> getTrends();

}