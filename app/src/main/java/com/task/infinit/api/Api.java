package com.task.infinit.api;

import com.task.infinit.App;
import com.task.infinit.util.Config;
import com.task.infinit.api.http.HttpUtils;
import com.task.infinit.api.logansquare.LoganSquareConverterFactory;
import com.task.infinit.api.service.InfinitService;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;

public final class Api {

    private static volatile InfinitService mService;

    public static InfinitService get() {
        InfinitService localInstance = mService;
        if (localInstance == null) {
            synchronized (InfinitService.class) {
                localInstance = mService;
                if (localInstance == null) {
                    mService = localInstance = createService();
                }
            }
        }

        return localInstance;
    }

    private static InfinitService createService() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.Api.URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(LoganSquareConverterFactory.create())
                .client(HttpUtils.createDefaultClient(App.get()))
                .build();

        return retrofit.create(InfinitService.class);
    }

    private Api() {
        // hide
    }
}