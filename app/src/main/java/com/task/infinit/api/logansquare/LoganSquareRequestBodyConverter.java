package com.task.infinit.api.logansquare;

import com.bluelinelabs.logansquare.LoganSquare;

import java.io.IOException;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Converter;

final class LoganSquareRequestBodyConverter implements Converter<Object, RequestBody> {

    private static final MediaType MEDIA_TYPE = MediaType.parse("application/json; charset=UTF-8");

    LoganSquareRequestBodyConverter() {
    }

    @Override
    public RequestBody convert(Object value) throws IOException {
        // For general cases, use the central LoganSquare serialization method
        return RequestBody.create(MEDIA_TYPE, LoganSquare.serialize(value));
    }
}
