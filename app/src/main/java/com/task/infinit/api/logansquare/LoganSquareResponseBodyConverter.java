package com.task.infinit.api.logansquare;

import com.bluelinelabs.logansquare.LoganSquare;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Converter;

final class LoganSquareResponseBodyConverter implements Converter<ResponseBody, Object> {

    private final Type type;

    LoganSquareResponseBodyConverter(Type type) {
        this.type = type;
    }

    @Override
    public Object convert(ResponseBody value) throws IOException {
        InputStream is = value.byteStream();

        try {
            return parse(is);
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private Object parse(InputStream is) throws IOException {
        if (type instanceof Class) {
            // Plain object conversion
            return LoganSquare.parse(is, (Class<?>) type);

        } else if (type instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) type;
            Type[] typeArguments = parameterizedType.getActualTypeArguments();
            Type firstType = typeArguments[0];

            // Check for Map arguments
            Type rawType = parameterizedType.getRawType();
            if (rawType == Map.class) {
                Type secondType = typeArguments[1];

                // Perform validity checks on the type arguments, since LoganSquare works only on String keys
                if (firstType == String.class && secondType instanceof Class) {
                    // Map conversion
                    return LoganSquare.parseMap(is, (Class<?>) secondType);
                }

            } else if (rawType == List.class && firstType instanceof Class) {
                return LoganSquare.parseList(is, (Class<?>) firstType);
            }
        }

        return null;
    }
}
