package com.task.infinit.api.http;

import okhttp3.logging.HttpLoggingInterceptor;
import timber.log.Timber;

public final class TimberHttpLogger implements HttpLoggingInterceptor.Logger {
    @Override
    public void log(String message) {
        Timber.tag("OkHttp").d(message);
    }
}
